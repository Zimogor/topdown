﻿using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;
using System;


public class DebugScript {

	[MenuItem("Debug/Debug/Run")]
	public static void RunDebug() {

		foreach (var go in Selection.gameObjects)
			go.transform.position *= 2;
	}

	[MenuItem("Debug/Retransform/RescaleSelectedAnimatorController")]
	public static void RescaleSelectedAnimatorController() {

		float scale = 2.0f;
		var objects = Selection.objects;
		if (objects.Length == 0) throw new Exception("Nothing is selected");
		if (objects.Length > 1) throw new Exception("Too many objects are selected");
		var controller = objects[0] as AnimatorController;
		if (!controller) throw new Exception("Something wrong is selected");

		for (int i = 0; i < controller.layers.Length; i++) {
			var stateMachine = controller.layers[i].stateMachine;
			foreach (var state in stateMachine.states)
				ScaleClip((AnimationClip)state.state.motion, scale);
		}
	}

	private static void ScaleClip(AnimationClip clip, float scale) {

		foreach (var cb in AnimationUtility.GetCurveBindings(clip)) {
			var oldCurve = AnimationUtility.GetEditorCurve(clip, cb);
			var newCurve = new AnimationCurve();

			for (int i = 0; i < oldCurve.keys.Length; i ++) {

				var key = oldCurve.keys[i];
				if (cb.propertyName == "m_IsActive" || cb.propertyName.Contains("localEulerAnglesRaw") || cb.propertyName.Contains("m_LocalScale"))
					newCurve.AddKey(key);
				else if (!cb.propertyName.Contains("m_LocalPosition"))
					throw new Exception(cb.propertyName);
				else {
					key.value *= scale;
					newCurve.AddKey(key);
				}
				newCurve.AddKey(key);
			}

			clip.SetCurve(cb.path, cb.type, cb.propertyName, newCurve);
		}
	}

	[MenuItem("Debug/Retransform/RescaleSelectedGameObject")]
	public static void RescaleSelectedGameObject() {

		float scale = 2.0f;
		scale = 1.0f / 1.28f;

		var gameObjects = Selection.gameObjects;
		if (gameObjects.Length == 0) throw new Exception("Nothing is selected");
		if (gameObjects.Length > 1) throw new Exception("Too many objects are selected");
		var gameObject = gameObjects[0];

		foreach (Transform child in gameObject.transform)
			ScaleGameObject(child.gameObject, scale);
	}

	private static void ScaleGameObject(GameObject gameObject, float scale) {

		gameObject.transform.localPosition = gameObject.transform.localPosition * scale;
		foreach (Transform child in gameObject.transform)
			ScaleGameObject(child.gameObject, scale);
	}
}
