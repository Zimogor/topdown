﻿using UnityEngine;
using MenuServiceLocator = MenuServiceLocation.ServiceLocator;
using LevelSeviceLocator = LevelServiceLocation.ServiceLocator;
using SceneManagement;


// порядок скриптов
// - GameManager - управление игрой
// - SceneLoader - управление загрузкой
// - LevelServiceLocator, MenuServiceLocator - снабжение сервисами
// - InputManager, KeyboardController, StickController - управление вводом (именно в таком порядке)
// - LevelManager - менеджмент уровнем

public class GameManager : MonoBehaviour {

	[SerializeField] SceneLoader sceneLoader = null;

	private void Awake() {

#if UNITY_ANDROID
		Application.targetFrameRate = 60;
#endif
		sceneLoader.OnMenuLoaded += OnMenuLoaded;
		sceneLoader.OnLevelLoaded += OnLevelLoaded;
	}

	private void OnMenuLoaded() {

		var menuManager = MenuServiceLocator.Instance.MenuManager;
		menuManager.OnPlayRequest += () => StartCoroutine(sceneLoader.LoadScene(SceneType.Level));
		menuManager.OnExitRequest += () => Application.Quit();
	}

	private void OnLevelLoaded() {

		LevelSeviceLocator.Instance.LevelManager.OnMainMenuRequest +=
			() => StartCoroutine(sceneLoader.LoadScene(SceneType.Menu));
	}
}
