﻿using UnityEngine;
using System;


namespace MenuManagement {

	public class MenuManager : MonoBehaviour {

		public Action OnPlayRequest;
		public Action OnExitRequest;

		public void OnPlayClick() => OnPlayRequest?.Invoke();
		public void OnExitClick() => OnExitRequest?.Invoke(); 

		private void OnDestroy() {
			
			OnPlayRequest = null;
			OnExitRequest = null;
		}
	}
}
