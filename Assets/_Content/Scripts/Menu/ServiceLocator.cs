﻿using UnityEngine;
using System;
using MenuManagement;


namespace MenuServiceLocation {

	public class ServiceLocator : MonoBehaviour {

		[SerializeField] MenuManager menuManager = null;

		public static ServiceLocator Instance;

		private void Awake() {
			if (Instance) throw new Exception();
			Instance = this;
		}

		private void OnDestroy() {
		
			Instance = null;
		}

		public MenuManager MenuManager => menuManager;
	}
}