﻿using UnityEngine;
using System;


namespace ScreenManagment {

	public class ScreenManager : MonoBehaviour {

		public Action<ScreenManager> OnResolutionChanged;
		public Vector2Int Resolution { get; private set; }
		public float ScaleFactor => canvas.scaleFactor;

		[SerializeField] Canvas canvas = null;

		private void Awake() {
		
			Resolution = new Vector2Int(Screen.width, Screen.height);
#if UNITY_ANDROID

			enabled = false;
#endif
		}

		// late, иначе scaleFactor не обновится
		private void LateUpdate() {
		
			if (Resolution.x != Screen.width || Resolution.y != Screen.height) {

				Resolution = new Vector2Int(Screen.width, Screen.height);
				OnResolutionChanged?.Invoke(this);
			}
		}

		private void OnDestroy() {
			
			OnResolutionChanged = null;
		}
	}
}