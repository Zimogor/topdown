﻿using UnityEngine;
using UnityEngine.Assertions;


public static class Utils {

	public static readonly int damagableLayerMask = 1 << LayerMask.NameToLayer("DamagableEnemy");
	public static readonly Collider[] nonAllockCollidersBuffer = new Collider[10];
	public static readonly Vector3 halfVector3 = new Vector3(0.5f, 0, 0.5f);
}

public class Randomizer<T> {

	private float totalWeight;
	private int size;
	private T[] values;
	private float[] weights;

	public Randomizer(T[] values, float[] weights) {
		Update(values, weights);
	}

	public void Remove(T value) {

		T[] newValues = new T[values.Length - 1];
		float[] newWeights = new float[weights.Length - 1];

		int newIndex = 0;
		for (int i = 0; i < values.Length; i ++) {
			if (values[i].Equals(value)) continue;
			newValues[newIndex] = values[i];
			newWeights[newIndex] = weights[i];
			newIndex ++;
		}

		Update(newValues, newWeights);
	}

	public void Update(T[] values, float[] weights) {
		Assert.IsTrue(values.Length == weights.Length);

		this.values = values;
		this.weights = weights;

		totalWeight = 0;
		size = values.Length;
		for (int i = 0; i < size; i ++) {
			totalWeight += weights[i];
			if (i > 0) weights[i] += weights[i - 1];
		}
	}

	public T GetStretched(float randValue) {

		randValue *= totalWeight;
		for (int i = 0; i < size; i++)
			if (randValue <= weights[i]) return values[i];
		return values[size - 1];
	}

	public T GetNullable(float randValue) {

		if (totalWeight > 1.0f) return GetStretched(randValue);
		else return GetSolid(randValue);
	}

	public T GetSolid(float randValue) {

		for (int i = 0; i < size; i++)
			if (randValue <= weights[i]) return values[i];
		return default;
	}
}
