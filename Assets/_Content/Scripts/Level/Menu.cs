﻿using UnityEngine;


public class Menu : MonoBehaviour {

	[SerializeField] GameObject menuButton = null;
	[SerializeField] GameObject buttonsBlock = null;

	private void Awake() {
		
		Hide();
	}

	public void Show() {

		menuButton.SetActive(false);
		buttonsBlock.SetActive(true);
	}

	public void Hide() {

		menuButton.SetActive(true);
		buttonsBlock.SetActive(false);
	}

	public bool Visible => buttonsBlock.activeInHierarchy;
}
