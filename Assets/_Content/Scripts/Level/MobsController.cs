﻿using UnityEngine;
using PoolSystem;
using PEventType = PoolSystem.EventType;


public class MobsController : MonoBehaviour {

	[SerializeField] Mob mobPrefab = null;
	[SerializeField] Transform poolTransform = null;
	[SerializeField] Transform runTransform = null;

	private Pool mobsPool;
	private int mobsCount = 0;

	private void Awake() {
		
		mobsPool = new Pool(mobPrefab.Poolable, poolTransform);
		mobsPool.OnEvent += PoolEvent;
	}

	public void SpawnMob(Vector3 position) {

		mobsPool.GetInstance(position, runTransform);
	}

	private void PoolEvent(PEventType eventType, Poolable poolable) {

		switch(eventType) {
			case PEventType.Spawn:
				mobsCount ++;
				break;
			case PEventType.Despawn:
				mobsCount --;
				break;
		}
	}

	private void OnDestroy() {
		
		mobsPool.OnDestroy();
	}
}
