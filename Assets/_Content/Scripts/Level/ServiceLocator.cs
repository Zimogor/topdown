﻿using UnityEngine;
using System;
using ScreenManagment;
using InputManagement;
using LevelManagement;


namespace LevelServiceLocation {

	public class ServiceLocator : MonoBehaviour {

		[SerializeField] ScreenManager screenManager = null;
		[SerializeField] LevelManager levelManager = null;
		[SerializeField] LevelGenerator levelGenerator = null;
		[SerializeField] Menu menu = null;
		[SerializeField] MobsController mobsController = null;

		public static ServiceLocator Instance;

		private void Awake() {
			if (Instance) throw new Exception();
			Instance = this;
		}

		private void OnDestroy() {
		
			Instance = null;
		}

		public void InstallInputController(InputController inputController) {

			InputController = inputController;
		}

		public ScreenManager ScreenManager => screenManager;
		public InputController InputController { get; private set; }
		public LevelManager LevelManager => levelManager;
		public Menu Menu => menu;
		public LevelGenerator LevelGenerator => levelGenerator;
		public MobsController MobsController => mobsController;
	}
}