﻿using UnityEngine;
using LevelServiceLocation;
using System;


namespace LevelManagement {

	public class LevelManager : MonoBehaviour {

		public Action OnMainMenuRequest;

		private void Awake() {
			
			ServiceLocator.Instance.InputController.OnOpenMenuRequest += OpenMenu;
			ServiceLocator.Instance.InputController.OnResumeMenuRequest += ResumeMenu;
			ServiceLocator.Instance.InputController.OnMainMenuRequest += () => OnMainMenuRequest?.Invoke();
			ServiceLocator.Instance.LevelGenerator.GenerateLevel();
		}

		private void OpenMenu() {

			ServiceLocator.Instance.Menu.Show();
		}

		private void ResumeMenu() {

			ServiceLocator.Instance.Menu.Hide();
		}

		private void OnDestroy() {
			
			OnMainMenuRequest = null;
		}
	}
}