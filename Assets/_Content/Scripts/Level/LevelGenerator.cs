﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Tilemaps;
using UnityEngine.AI;


public class LevelGenerator : MonoBehaviour {

	[SerializeField] float externalTreesRadius = 0.0f;
	[SerializeField] float internalTreesRadius = 5.0f;
	[SerializeField] float externalTreesDensity = 0.1f;
	[SerializeField] float internalTreesDensity = 0.01f;
	[SerializeField] float externalMushroomsDensity = 0.1f;
	[SerializeField] float externalGrassDensity = 0.1f;
	[SerializeField] float internalMushroomsDensity = 0.1f;
	[SerializeField] float internalGrassDensity = 0.1f;

	[SerializeField] NavMeshAgent player = null;
	[SerializeField] BoxCollider camConfiner = null;
	[SerializeField] NavMeshSurface navMesh = null;
	[SerializeField] Tilemap tilemap = null;
	[SerializeField] BoxCollider mainCollider = null;
	[SerializeField] Tile grassTile = null;
	[SerializeField] Tile flowersTile = null;
	[SerializeField] GameObject fencePrefab = null;
	[SerializeField] GameObject stakesPrefab = null;
	[SerializeField] GameObject treePrefab = null;
	[SerializeField] GameObject mushroomPrefab = null;
	[SerializeField] GameObject grassPrefab = null;
	[SerializeField] Spawner spawnerPrefab = null;
	[SerializeField] Transform container = null;

	private struct FloraGenerationData {

		public IEnumerable<Vector2Int> positions;
		public float treesDensity;
		public float treesRadius;
		public float mushroomsDensity;
		public float grassDensity;
	}

	public void GenerateLevel() {

		var buisySet = new HashSet<Vector2Int>();
		var internalSet = new HashSet<Vector2Int>();
		var externalSet = new HashSet<Vector2Int>();

		var arenaFrom = new Vector2Int(-25, 1);
		var arenaTo = new Vector2Int(10, 15);
		VectorsToCorners(ref arenaFrom, ref arenaTo);

		var rect = new RectInt(arenaFrom, arenaTo - arenaFrom);
		GenerateFence(rect, buisySet);
		var spawner = Instantiate(spawnerPrefab, new Vector3(rect.xMax, 0, rect.center.y) + Vector3.left * 5, Quaternion.identity, container);
		spawner.radius = 5.0f;
		spawner.count = 3;
		var pos = new Vector2Int();
		for (pos.x = rect.xMin; pos.x <= rect.xMax; pos.x ++)
			for (pos.y = rect.yMin; pos.y <= rect.yMax; pos.y ++)
				internalSet.Add(pos);

		int bottom_space = 8;
		int top_space = 12;
		int side_space = 17;
		rect.SetMinMax(rect.min - new Vector2Int(side_space, bottom_space), rect.max + new Vector2Int(side_space, top_space));
		GenerateGrass(rect);
		for (pos.x = rect.xMin; pos.x <= rect.xMax; pos.x ++)
			for (pos.y = rect.yMin; pos.y <= rect.yMax; pos.y ++)
				if (!internalSet.Contains(pos))
					externalSet.Add(pos);
		internalSet.ExceptWith(buisySet);
		externalSet.ExceptWith(buisySet);

		var data = new FloraGenerationData() {
			positions = externalSet,
			treesDensity = externalTreesDensity,
			treesRadius = externalTreesRadius,
			mushroomsDensity = externalMushroomsDensity,
			grassDensity = externalGrassDensity
		};
		GenerateFlora(data, buisySet);
		externalSet.ExceptWith(buisySet);
		internalSet.ExceptWith(buisySet);

		data = new FloraGenerationData() {
			positions = internalSet,
			treesDensity = internalTreesDensity,
			treesRadius = internalTreesRadius,
			mushroomsDensity = internalMushroomsDensity,
			grassDensity = internalGrassDensity
		};
		GenerateFlora(data, buisySet);
		externalSet.ExceptWith(buisySet);
		internalSet.ExceptWith(buisySet);

		GenerateNavigation(rect);
		player.Warp(new Vector3(arenaFrom.x + 2, 0, (arenaFrom.y + arenaTo.y) * 0.5f));
	}

	private void GenerateGrass(RectInt rect) {

		var noise = new FastNoise(Random.Range(0, int.MaxValue));
		noise.SetFrequency(0.4f);

		container.SetParent(transform);
		var posInt = new Vector3Int();
		for (posInt.x = rect.xMin; posInt.x <= rect.xMax; posInt.x++)
			for (posInt.y = rect.yMin; posInt.y <= rect.yMax; posInt.y++) {
				posInt.z = 0;
				tilemap.SetTile(posInt, grassTile);
				if (noise.GetValue(posInt.x, posInt.y) > 0.5) {
					posInt.z = 1;
					tilemap.SetTile(posInt, flowersTile);
				}
			}
	}

	private void GenerateFence(RectInt fenceRect, HashSet<Vector2Int> buisySet) {

		var stakesPositionScatter = new Vector2(0.2f, 0.2f);
		var stakesScaleScatter = new Vector2(0.8f, 1.2f);
		var stakesSquashScatter = new Vector2(0.9f, 1.2f);

		var posInt = new Vector3Int(0, 0, fenceRect.yMin);
		for (posInt.x = fenceRect.xMin; posInt.x <= fenceRect.xMax; posInt.x++) {
			buisySet.Add(new Vector2Int(posInt.x, posInt.z));
			Instantiate(fencePrefab, posInt + Utils.halfVector3, Quaternion.identity, container);
		}
		posInt.z = fenceRect.yMax;
		for (posInt.x = fenceRect.xMin; posInt.x <= fenceRect.xMax; posInt.x++) {
			buisySet.Add(new Vector2Int(posInt.x, posInt.z));
			Instantiate(fencePrefab, posInt + Utils.halfVector3, Quaternion.identity, container);
		}
		for (int x = fenceRect.xMin; x <= fenceRect.xMax; x += fenceRect.size.x) {
			posInt.Set(x, 0, 0);
			for (posInt.z = fenceRect.yMin + 1; posInt.z < fenceRect.yMax; posInt.z++) {
				Vector3 pos = posInt + Utils.halfVector3;
				var stake = Instantiate(stakesPrefab, pos, Quaternion.identity, container);
				RandomizeGO(stake, stakesPositionScatter, stakesScaleScatter, stakesSquashScatter);
				buisySet.Add(new Vector2Int(posInt.x, posInt.z));
			}
		}
	}

	private void RandomizeGO(GameObject gameObject, Vector2 positionScatter, Vector2 scaleScatter, Vector2 squashScatter) {

		Vector3 pos = gameObject.transform.position;
		pos.x += Random.Range(-positionScatter.x, positionScatter.x);
		pos.z += Random.Range(-positionScatter.y, positionScatter.y);
		var scale = Random.Range(scaleScatter.x, scaleScatter.y);
		var squash = Random.Range(squashScatter.x, squashScatter.y);
		gameObject.transform.localScale = new Vector3(scale, scale * squash, 1.0f);
		gameObject.transform.position = pos;
	}

	private void GenerateNavigation(RectInt rect) {
		
		// навигация
		mainCollider.size = new Vector3Int(rect.width, 1, rect.height);
		mainCollider.center = new Vector3(rect.center.x, -0.5f, rect.center.y);
		navMesh.BuildNavMesh();

		// camconfiner
		camConfiner.size = new Vector3(mainCollider.size.x, 0, Mathf.Max(0, mainCollider.size.z - 23));
		camConfiner.center = mainCollider.center + Vector3.up * (10 + mainCollider.size.y * 0.5f) + Vector3.back * 16.5f;
	}

	private void GenerateFlora(FloraGenerationData data, HashSet<Vector2Int> buisySet) {

		var positionScatter = new Vector2(0.2f, 0.2f);
		var scaleScatter = new Vector2(0.8f, 1.2f);
		var squashScatter = new Vector2(0.9f, 1.2f);

		var values = new GameObject[] { mushroomPrefab, treePrefab, grassPrefab };
		var weights = new float[] { data.mushroomsDensity, data.treesDensity, data.grassDensity };
		var randomizer = new Randomizer<GameObject>(values, weights);

		var treesSet = new HashSet<Vector2Int>();

		var pos = new Vector3();
		var posInt2 = new Vector2Int();
		foreach (var posInt in data.positions) {
			if (buisySet.Contains(posInt)) continue;

			var gameObject = randomizer.GetSolid(Random.value);
			if (!gameObject) continue;

			if (data.treesRadius > 0 && gameObject == treePrefab) {

				bool hasNear = false;
				for (posInt2.x = Mathf.RoundToInt(posInt.x - data.treesRadius); posInt2.x <= Mathf.RoundToInt(posInt.x + data.treesRadius); posInt2.x ++)
					for (posInt2.y = Mathf.RoundToInt(posInt.y - data.treesRadius); posInt2.y <= Mathf.RoundToInt(posInt.y + data.treesRadius); posInt2.y ++) {
						if (Vector2.Distance(posInt, posInt2) > data.treesRadius) continue;
						if (treesSet.Contains(posInt2)) {
							hasNear = true;
							goto BreakOut;
						}
					}
				BreakOut:
				if (hasNear) continue;
				treesSet.Add(posInt);
			}
			
			pos.Set(posInt.x, 0, posInt.y);
			pos.x += (posInt.y % 2 == 0) ? 0.75f : 0.25f;
			var obj = Instantiate(gameObject, pos, Quaternion.identity, container);
			buisySet.Add(posInt);
			RandomizeGO(obj, positionScatter, scaleScatter, squashScatter);
		}
	}

	private void VectorsToCorners(ref Vector2Int v1, ref Vector2Int v2) {

		var buf = v2;
		v1.Set(Mathf.Min(v1.x, v2.x), Mathf.Min(v1.y, v2.y));
		v2.Set(Mathf.Min(buf.x, v2.x), Mathf.Min(buf.y, v2.y));
	}
}
