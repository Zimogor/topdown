﻿using UnityEngine;
using LevelServiceLocation;
using System.Collections;
using UnityEngine.AI;


public class Spawner : MonoBehaviour {

	public float radius = 1.0f;
	public int count = 3;

	private IEnumerator Start() {

		int remain = count;
		while(remain > 0) {

			remain -= SpawnBlock(Mathf.Min(remain, 5));
			yield return null;
		}

		Destroy(gameObject);
	}

	private int SpawnBlock(int amount) {

		var mobsController = ServiceLocator.Instance.MobsController;
		for (int i = 0; i < amount; i ++) {

			float seekDistance = 1.0f;
			Vector2 randomPoint = new Vector2(transform.position.x, transform.position.z);
			randomPoint += Random.insideUnitCircle * (radius - seekDistance);
			var target = new Vector3(randomPoint.x, 0, randomPoint.y);
			if (NavMesh.SamplePosition(target, out NavMeshHit sampleHit, seekDistance, NavMesh.AllAreas)) {
				if (NavMesh.Raycast(transform.position, target, out NavMeshHit raycastHit, NavMesh.AllAreas))
					return i;
				mobsController.SpawnMob(sampleHit.position);

			} else return i;
		}

		return amount;
	}

	private void OnDrawGizmos() {
		
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, radius);
	}
}
