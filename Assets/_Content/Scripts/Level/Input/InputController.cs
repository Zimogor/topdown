﻿using UnityEngine;
using System;


namespace InputManagement {

	public interface IListener {

		void OnToggleMenuEvent();
	}

	public class InputController : MonoBehaviour {

		public Vector2 Direction { get; protected set; }
		public bool JustStrike { get; protected set; } = false;

		public Action OnOpenMenuRequest;
		public Action OnResumeMenuRequest;
		public Action OnMainMenuRequest;

		public IListener Listener { protected get; set; }

		public void OpenMenuRequest() => OnOpenMenuRequest?.Invoke();
		public void ResumeMenuRequest() => OnResumeMenuRequest?.Invoke();
		public void MainMenuRequest() => OnMainMenuRequest?.Invoke(); 

		private void OnDestroy() {
			
			Listener = null;
			OnOpenMenuRequest = null;
			OnResumeMenuRequest = null;
			OnMainMenuRequest = null;
		}
	}
}
