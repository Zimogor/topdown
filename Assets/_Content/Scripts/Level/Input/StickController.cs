﻿using UnityEngine;
using LevelServiceLocation;
using UnityEngine.EventSystems;


namespace InputManagement {

	public class StickController : InputController {

		[SerializeField] GameObject stickBase = null;
		[SerializeField] GameObject stickPoint = null;
		[SerializeField] float deadZone = 0.1f;

		private float radius = 0.0f;
		private int? touchFingerID = null;

		private void Awake() {
		
			stickBase.SetActive(false);
		}

		private void Start() {
		
			var screenManager = ServiceLocator.Instance.ScreenManager;
			screenManager.OnResolutionChanged += _ => AdaptResolution(screenManager.ScaleFactor);
			AdaptResolution(screenManager.ScaleFactor);
		}

		private void AdaptResolution(float scaleFactor) {

			radius = ((RectTransform)stickBase.transform).sizeDelta.y * scaleFactor * 0.5f;
		}

		private void Update() {

			Direction = Vector2.zero;
			JustStrike = false;
			if (Input.touchCount == 0) touchFingerID = null;

			foreach (var touch in Input.touches) {
				switch (touch.phase) {

					case TouchPhase.Began:
						if (!EventSystem.current.IsPointerOverGameObject(touch.fingerId))
							if (touch.position.x < Screen.width * 0.5f) {

								// слева
								if (touchFingerID == null) {

									touchFingerID = touch.fingerId;
									stickBase.transform.position = touch.position;
								}

							} else {

								// справа
								JustStrike = true;
							}
						break;

					case TouchPhase.Ended:
						if (touch.fingerId == touchFingerID)
							touchFingerID = null;
						break;
				}

				if (touch.fingerId != touchFingerID) continue;

				stickPoint.transform.position = touch.position;
				Vector2 baseToPoint = stickPoint.transform.position - stickBase.transform.position;
				float distance = baseToPoint.magnitude;
				if (distance > 0.0f) {

					baseToPoint.Normalize();
					float moveDistance = distance - radius;
					if (moveDistance > 0.0f)
						stickBase.transform.Translate(baseToPoint * moveDistance);
					if (distance / radius > deadZone)
						Direction = baseToPoint;
				}
			}

			stickBase.SetActive(touchFingerID != null);
		}
	}

}