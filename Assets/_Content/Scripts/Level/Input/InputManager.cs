﻿using UnityEngine;
using LevelServiceLocation;


namespace InputManagement {

	public class InputManager : MonoBehaviour, IListener {

		[SerializeField] StickController stickController = null;
		[SerializeField] KeyboardController keyboardController = null;

		private InputController curController;

		private void Awake() {

#if UNITY_STANDALONE

			curController = keyboardController;
			keyboardController.gameObject.SetActive(true);
			stickController.gameObject.SetActive(false);

#elif UNITY_ANDROID

			curController = stickController;
			keyboardController.gameObject.SetActive(false);
			stickController.gameObject.SetActive(true);

#endif
			curController.Listener = this;
			ServiceLocator.Instance.InstallInputController(curController);
		}

		void IListener.OnToggleMenuEvent() {
			if (ServiceLocator.Instance.Menu.Visible)
				curController.ResumeMenuRequest();
			else
				curController.OpenMenuRequest();
		}
		public void OnMenuClick() {
			curController.OpenMenuRequest();
		}
		public void OnResumeClick() {
			curController.ResumeMenuRequest();
		}
		public void OnMainMenuClick() {
			curController.MainMenuRequest();
		}
	}

}
