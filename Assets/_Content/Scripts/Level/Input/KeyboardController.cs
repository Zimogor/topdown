﻿using UnityEngine;


namespace InputManagement {

	public class KeyboardController : InputController {

		private float lastAngleTime = float.MinValue;
		private Vector2 lastAngleValue = new Vector2();
		private const float restoreAngleTime = 0.03f;

		private void Update() {
		
			JustStrike = false;
			if (Input.GetButtonDown("Fire2"))
				JustStrike = true;

			var hor = Input.GetAxisRaw("Horizontal");
			var vert = Input.GetAxisRaw("Vertical");

			if (Mathf.Abs(hor) == 1.0f && Mathf.Abs(vert) == 1.0f) {
				lastAngleTime = Time.time;
				lastAngleValue.Set(hor, vert);

			} else if (hor == 0.0f && vert == 0.0f && Time.time - lastAngleTime <= restoreAngleTime) {

				hor = lastAngleValue.x;
				vert = lastAngleValue.y;
			}

			Direction = new Vector2(hor, vert);
			if (Direction != Vector2.zero)
				Direction = Direction.normalized;

			if (Input.GetButtonDown("Cancel")) {
				Listener?.OnToggleMenuEvent();
			}
		}
	}
}