﻿using UnityEngine;
using System;

namespace AnimatorRecieving {

	public enum EventType { StrikePeak, DeathFade }

	public class AnimationReciever : MonoBehaviour {

		public Action<EventType> OnEvent;

		public void OnStrikePeakAnimation() {

			OnEvent?.Invoke(EventType.StrikePeak);
		}

		public void OnDeathFadeAnimation() {

			OnEvent?.Invoke(EventType.DeathFade);
		}

		private void OnDestroy() {
		
			OnEvent = null;
		}
	}
}