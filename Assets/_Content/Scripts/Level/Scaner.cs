﻿using UnityEngine;
using System.Collections.Generic;
using Damaging;


public class Scaner : MonoBehaviour {

	private List<Damagable> targets = new List<Damagable>();

	private void OnTriggerEnter(Collider other) {
		
		var target = other.GetComponent<Damagable>();
		targets.Add(target);
	}

	private void OnTriggerExit(Collider other) {
		
		var target = other.GetComponent<Damagable>();
		targets.Remove(target);
	}

	private void OnDisable() {
		
		targets.Clear();
	}

	public Damagable AnyTarget => targets.Count > 0 ? targets[0] : null;
}
