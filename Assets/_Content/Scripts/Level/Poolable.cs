﻿using UnityEngine;
using System;


namespace PoolSystem {

	public class Poolable : MonoBehaviour {

		public Action<EventType> OnEvent;

		[SerializeField] int poolIndex = 0;

		private Pool pool = null;

		public void GoToPool() {

			if (pool != null) pool.Free(this);
			else Destroy(gameObject);
		}

		public void OnDespawn(Pool pool) {

			this.pool = pool;
			OnEvent?.Invoke(EventType.Despawn);
		}

		public void OnSpawn(Pool pool) {

			this.pool = pool;
			OnEvent?.Invoke(EventType.Spawn);
		}

		public int PoolIndex => poolIndex;

		private void OnDestroy() {
		
			pool = null;
			OnEvent = null;
		}
	}

}