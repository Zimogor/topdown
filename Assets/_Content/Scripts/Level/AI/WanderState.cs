﻿using UnityEngine;
using UnityEngine.AI;


namespace StateMachines {

	public interface IWanderStateUser {

		NavMeshAgent NavMeshAgent { get; }
		Vector3 Pos { get; }
	}

	public class WanderState : State {

		private float idleTime;
		private IWanderStateUser user;

		private const float sampleDistance = 5.0f;
		private static readonly Vector2 successWaitTime = new Vector2(2.0f, 4.0f);
		private static readonly Vector2 unsuccessWaitTime = new Vector2(0.0f, 2.0f);

		public WanderState(IWanderStateUser user) {

			this.user = user;
		}

		public override void OnEnter() {
			base.OnEnter();
			
			idleTime = Random.Range(0.0f, successWaitTime.y - successWaitTime.x);
		}

		public override void Update() {

			idleTime -= Time.deltaTime;
			if (idleTime <= 0.0f) {

				var randomDirection = Random.insideUnitCircle * sampleDistance;
				var destination = user.Pos + new Vector3(randomDirection.x, 0.0f, randomDirection.y);
				for (int i = 0; i < 5; i ++) {
					if (NavMesh.SamplePosition(destination, out NavMeshHit hit, sampleDistance, NavMesh.AllAreas)) {

						user.NavMeshAgent.destination = hit.position;
						idleTime = Random.Range(successWaitTime.x, successWaitTime.y);
						break;
					}
				}
				if (idleTime <= 0.0f) idleTime = Random.Range(unsuccessWaitTime.x, unsuccessWaitTime.y);
			}
		}
	}

}