﻿using UnityEngine;
using UnityEngine.AI;


namespace StateMachines {

	public interface IAttackStateUser {

		Scaner Scaner { get; }
		NavMeshAgent NavMeshAgent { get; }
		Vector3 Pos { get; }
	}

	public class AttackState : State {

		private IAttackStateUser user;
		private const float attackDistance = 1.5f;

		public AttackState(IAttackStateUser user) {

			this.user = user;
		}

		public override void OnEnter() {
			base.OnEnter();
			
			user.NavMeshAgent.isStopped = true;
			user.NavMeshAgent.ResetPath();
		}

		public bool HasTarget {
			get {

				var target = user.Scaner.AnyTarget;
				if (!target) return false;
				return Vector3.Distance(target.transform.position, user.Pos) <= attackDistance;
			}
		}
	}
}