﻿using UnityEngine;
using UnityEngine.AI;


namespace StateMachines {

	public interface IPersueStateUser {

		Scaner Scaner { get; }
		NavMeshAgent NavMeshAgent { get; }
	}

	public class PersueState : State {

		private IPersueStateUser user;
		private float nextRunTime;
		private const float runPeriod = 0.2f;
		private const float strikeDistance = 1.5f;

		public PersueState(IPersueStateUser user) {

			this.user = user;
		}

		public override void OnEnter() {
			base.OnEnter();
			
			RunToTarget();
		}

		private void RunToTarget() {

			nextRunTime = Time.time + runPeriod;
			user.NavMeshAgent.destination = user.Scaner.AnyTarget.transform.position;
		}

		public override void Update() {
			
			var target = user.Scaner.AnyTarget;
			if (!target) return;
			if (Time.time >= nextRunTime) RunToTarget();
		}

		public bool HasTarget => user.Scaner.AnyTarget;
	}
}