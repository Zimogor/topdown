﻿using UnityEngine;
using System;


namespace StateMachines {

	public delegate bool Trigger();

	public class StateMachine {

		public Action<State, State> OnStateChangedEvent; // new, old

		private State currentState;
		private State[] states;

		public StateMachine(State[] states) {

			this.states = states;
		}

		public void Update() {

			foreach (var state in states)
				state.Think();

			Transition triggeredTransition = null;
			foreach (var transition in currentState.transitions)
				if (transition.isTriggered()) {
					triggeredTransition = transition;
					break;
				}

			if (triggeredTransition == null) {

				currentState.Update();

			} else {

				var oldState = currentState;
				oldState.OnExit();
				currentState = triggeredTransition.targetState;
				currentState.OnEnter();
				OnStateChangedEvent?.Invoke(currentState, oldState);
			}
		}

		public void Reset() {

			foreach (var state in states)
				state.Reset();
			currentState = states[0];
			currentState.OnEnter();
			OnStateChangedEvent?.Invoke(currentState, null);
		}

		public void OnDestroy() {

			OnStateChangedEvent = null;
		}
	}

	public class State {

		public Transition[] transitions;

		public virtual void Think() { }
		public virtual void OnEnter() { }
		public virtual void OnExit() { }
		public virtual void Update() { }
		public virtual void Reset() { }
	}

	public class Transition {

		public Trigger isTriggered;
		public State targetState;
	}
}