﻿using UnityEngine;
using System.Collections;
using System;
using AnimatorRecieving;
using AREventType = AnimatorRecieving.EventType;

namespace Damaging {

	public enum EventType { Die, FadedAway }

	public class Damagable : MonoBehaviour {

		public Action<EventType> OnEvent;

		[SerializeField] int maxLife = 100;
		[SerializeField] AnimationReciever animationReciever = null;
		[SerializeField] SpriteRenderer[] twinkleSprites = null;
		[SerializeField] SpriteRenderer[] fadeSprites = null;
		[SerializeField] Material standardMaterial = null;
		[SerializeField] Material whiteMaterial = null;

		private Coroutine twinkleCoroutine = null;
		private int life;

		private void Awake() {
		
			if (animationReciever) animationReciever.OnEvent += OnAnimationEvent;
		}

		private void OnEnable() {
			
			life = maxLife;
			if (twinkleCoroutine != null) StopCoroutine(twinkleCoroutine);
			twinkleCoroutine = null;
			foreach (var sprite in fadeSprites) {
				sprite.material = standardMaterial;
				sprite.color = Color.white;
			}
		}

		public void AfflictDamage(int value) {
			if (Dead) return;

			life = Mathf.Max(0, life - value);
			if (life == 0.0f) OnEvent?.Invoke(EventType.Die);

			if (twinkleCoroutine != null) StopCoroutine(twinkleCoroutine);
			twinkleCoroutine = StartCoroutine(MakeWhite());
		}

		private void OnAnimationEvent(AREventType eventType) {

			switch (eventType) {
				case AREventType.DeathFade:
					StartCoroutine(DeathFadeCoroutine());
					break;
			}
		}

		private IEnumerator DeathFadeCoroutine() {

			float factor = 0.0f;
			float fadeSpeed = 1.0f;
			Color startColor = Color.white;
			Color endColor = new Color(0, 0, 0, 0);

			while (factor < 1.0f) {

				factor = Mathf.Min(1.0f, factor + Time.deltaTime * fadeSpeed);
				var color = Color.Lerp(startColor, endColor, factor);
				foreach (var sprite in fadeSprites)
					sprite.color = color;
				yield return null;
			}

			OnEvent?.Invoke(EventType.FadedAway);
		}

		private IEnumerator MakeWhite() {

			foreach (var sprite in twinkleSprites)
				sprite.material = whiteMaterial;
			yield return new WaitForSeconds(0.1f);
			foreach (var sprite in twinkleSprites)
				sprite.material = standardMaterial;
			twinkleCoroutine = null;
		}

		private void OnDestroy() {
		
			OnEvent = null;
		}

		public bool Dead => life == 0.0f;
	}
}