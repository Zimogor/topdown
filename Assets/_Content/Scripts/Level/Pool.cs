﻿using UnityEngine;
using System;
using Object = UnityEngine.Object;
using System.Collections.Generic;


namespace PoolSystem {

	public enum EventType { Spawn, Despawn }


	public class Pool {

		public Action<EventType, Poolable> OnEvent;

		private List<Poolable> poolables = new List<Poolable>();
		private Poolable poolable;
		private Transform poolTransform;

		public Pool(Poolable poolable, Transform poolTransform) {

			this.poolTransform = poolTransform;
			this.poolable = poolable;
		}

		public Poolable GetInstance(Vector3 position, Transform runTransform) {

			Poolable poolable = null;
			if (poolables.Count == 0)
				poolable = Object.Instantiate(this.poolable, position, Quaternion.identity, runTransform);
			else {
				poolable = poolables[poolables.Count - 1];
				poolables.RemoveAt(poolables.Count - 1);
				poolable.transform.SetParent(runTransform, false);
				poolable.transform.position = position;
			}

			poolable.gameObject.SetActive(true);
			poolable.OnSpawn(this);
			OnEvent?.Invoke(EventType.Spawn, poolable);
			return poolable;
		}

		public void Free(Poolable poolable) {

			poolable.OnDespawn(this);
			poolable.gameObject.SetActive(false);
			poolable.transform.SetParent(poolTransform, false);
			poolables.Add(poolable);
			OnEvent?.Invoke(EventType.Despawn, poolable);
		}

		public void OnDestroy() {

			OnEvent = null;
		}
	}
}
