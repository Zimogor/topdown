﻿using UnityEngine;
using UnityEngine.AI;
using StateMachines;
using ObjectRotationSystem;
using Damaging;
using DEventType = Damaging.EventType;
using PEventType = PoolSystem.EventType;
using AREventType = AnimatorRecieving.EventType;
using PoolSystem;
using AnimatorRecieving;


public class Mob : MonoBehaviour, IWanderStateUser, IPersueStateUser, IAttackStateUser {

	[SerializeField] NavMeshAgent navMeshAgent = null;
	[SerializeField] Animator animator = null;
	[SerializeField] Rotator rotator = null;
	[SerializeField] Damagable damagable = null;
	[SerializeField] Poolable poolable = null;
	[SerializeField] Scaner scaner = null;
	[SerializeField] AnimationReciever animationReciever = null;

	private StateMachine stateMachine;
	private AttackState attackState;

	private void Awake() {

		damagable.OnEvent += OnDamagableEvent;
		poolable.OnEvent += OnPoolableEvent;
		navMeshAgent.updateRotation = false;
		animationReciever.OnEvent += OnAnimationEvent;

		var wanderState = new WanderState(this);
		var deadState = new DeadState();
		var persueState = new PersueState(this);
		attackState = new AttackState(this);

		var anyToDeadTransition = new Transition() {
			targetState = deadState,
			isTriggered = () => damagable.Dead
		};
		var anyToPersueTransition = new Transition() {
			targetState = persueState,
			isTriggered = () => !attackState.HasTarget && persueState.HasTarget
		};
		var anyToWanderTransition = new Transition() {
			targetState = wanderState,
			isTriggered = () => !persueState.HasTarget && !attackState.HasTarget
		};
		var anyToAttackTransition = new Transition() {
			targetState = attackState,
			isTriggered = () => attackState.HasTarget
		};

		wanderState.transitions = new Transition[] {
			anyToDeadTransition, anyToAttackTransition, anyToPersueTransition
		};
		persueState.transitions = new Transition[] {
			anyToDeadTransition, anyToAttackTransition, anyToWanderTransition
		};
		attackState.transitions = new Transition[] {
			anyToDeadTransition, anyToPersueTransition, anyToWanderTransition
		};
		deadState.transitions = new Transition[] { };
		
		var states = new State[] { wanderState, persueState, attackState, deadState };
		stateMachine = new StateMachine(states);
		stateMachine.OnStateChangedEvent += OnStateChanged;
	}

	private void OnAnimationEvent(AREventType eventType) {

		switch(eventType) {
			case AREventType.StrikePeak:
				scaner.AnyTarget?.AfflictDamage(0);
				break;
		}
	}

	private void OnStateChanged(State state, State oldState) {

		if (state == attackState) animator.SetBool("StrikeSimple", true);
		else if (oldState == attackState) animator.SetBool("StrikeSimple", false);
	}

	private void OnPoolableEvent(PEventType eventType) {

		switch(eventType) {
			case PEventType.Spawn:
				navMeshAgent.enabled = true;
				animator.enabled = true;
				stateMachine.Reset();
				break;
		}
	}

	private void OnDamagableEvent(DEventType eventType) {

		switch(eventType) {
			case DEventType.Die:
				animator.SetTrigger("Die");
				navMeshAgent.isStopped = true;
				navMeshAgent.ResetPath();
				navMeshAgent.enabled = false;
				break;
			case DEventType.FadedAway:
				animator.WriteDefaultValues();
				animator.enabled = false;
				poolable.GoToPool();
				navMeshAgent.enabled = true;
				break;
		}
	}

	private void Update() {
		
		stateMachine.Update();
		animator.SetFloat("Speed", navMeshAgent.velocity.magnitude);
		rotator.SetLookat(navMeshAgent.steeringTarget);
	}

	private void OnDestroy() {
		
		stateMachine.OnDestroy();
	}

	public Scaner Scaner => scaner;
	public NavMeshAgent NavMeshAgent => navMeshAgent;
	public Vector3 Pos => transform.position;
	public Poolable Poolable => poolable;
}
