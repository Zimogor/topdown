﻿using UnityEngine;
using System.Collections;


namespace ObjectRotationSystem {

	public enum Direction { Right, Left }

	public class Rotator : MonoBehaviour {

		public Direction Direction { get; private set; }

		[SerializeField] private bool inverted = false;

		private Coroutine coroutine = null;
		private const float angularVelocity = 720.0f;

		private void Awake() {
		
			Direction = inverted ? Direction.Left : Direction.Right;
		}

		public void SetLookat(Vector3 position) {

			SetDirection(position - transform.position);
		}

		public void SetDirection(Vector3 direction) {

			if (direction.x == 0) return;
			Direction neededDirection = direction.x > 0 ? Direction.Right : Direction.Left;
			if (neededDirection == this.Direction) return;

			this.Direction = neededDirection;
			if (coroutine != null) return;
			coroutine = StartCoroutine(RotateCoroutine());
		}

		private IEnumerator RotateCoroutine() {

			while(true) {

				var curRotation = transform.localRotation;

				bool k = inverted == (Direction == Direction.Right);
				Quaternion neededRotation = Quaternion.Euler(0, k ? 180 : 0, 0);
				var result = Quaternion.RotateTowards(curRotation, neededRotation, angularVelocity * Time.deltaTime);
				if (result == transform.localRotation) break;
				transform.localRotation = result;
				yield return null;
			}

			coroutine = null;
		}
	}

}