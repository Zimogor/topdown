﻿using UnityEngine;
using InputManagement;
using LevelServiceLocation;
using UnityEngine.AI;
using ObjectRotationSystem;
using AnimatorRecieving;
using AREventType = AnimatorRecieving.EventType;
using Damaging;


public class Player : MonoBehaviour {

	[SerializeField] float speed = 0.0f;
	[SerializeField] Animator animator = null;
	[SerializeField] NavMeshAgent agent = null;
	[SerializeField] Rotator rotator = null;
	[SerializeField] AnimationReciever animationReciever = null;

	private InputController inputController;
	private float meleeSway;
	private Vector2 lastDirection;
	private float swayVelocity;
	private float lastStrikeTime;

	private const float strikeDelay = 0.5f;

	private void Awake() {

		lastStrikeTime = float.MinValue;
		meleeSway = 0.5f;
		lastDirection = Vector2.right;
		agent.updateRotation = false;
		inputController = ServiceLocator.Instance.InputController;
		animationReciever.OnEvent += OnAnimationEvent;
	}

	private void Update() {
		
		Vector2 direction = inputController.Direction * speed;
		Vector3 velocity = new Vector3(direction.x, 0.0f, direction.y);
		agent.velocity = velocity;

		animator.SetFloat("Speed", velocity.magnitude);
		if (direction != Vector2.zero)
			lastDirection = direction;
		float newMeleeSway = Vector2.Angle(Vector2.up, lastDirection);
		newMeleeSway = newMeleeSway == 180.0f ? 0.99f : newMeleeSway / 180.0f;
		meleeSway = Mathf.SmoothDamp(meleeSway, newMeleeSway, ref swayVelocity, 0.2f);

		animator.SetFloat("MeleeSway", meleeSway);
		rotator.SetDirection(velocity);

		if (inputController.JustStrike && Time.time >= lastStrikeTime + strikeDelay) {

			lastStrikeTime = Time.time;
			animator.SetTrigger("StrikeSway");
		}
	}

	private void OnAnimationEvent(AREventType eventType) {

		switch(eventType) {
			case AREventType.StrikePeak:
				Strike();
				break;
		}
	}

	private void Strike() {

		var direction = new Vector3(Mathf.Sin(meleeSway * Mathf.PI), 0.0f, Mathf.Cos(meleeSway * Mathf.PI)) * 1.5f;
		if (rotator.Direction == Direction.Left) direction.x *= -1;

		var startPos = transform.position + direction;
		var endPos = startPos + Vector3.up * 2.0f;

		int damagableCount = Physics.OverlapCapsuleNonAlloc(startPos, endPos, 0.5f, Utils.nonAllockCollidersBuffer, Utils.damagableLayerMask);
		for (int i = 0; i < damagableCount; i ++) {
			var damagable = Utils.nonAllockCollidersBuffer[i].GetComponent<Damagable>();
			damagable?.AfflictDamage(20);
		}
	}
}
