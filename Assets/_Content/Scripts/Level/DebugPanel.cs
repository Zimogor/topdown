﻿using UnityEngine;
using UnityEngine.UI;


public class DebugPanel : MonoBehaviour {

	[SerializeField] Text fpsText = null;

	private int fpsCounter = 0;
	private float floatFPSCountTime = 0.0f;

	private const string fpsPrefix = "FPS: ";

	private void Update() {
		
		floatFPSCountTime += Time.deltaTime;
		fpsCounter ++;
		if (floatFPSCountTime >= 1.0f) {

			fpsText.text = fpsPrefix + fpsCounter;
			floatFPSCountTime -= 1.0f;
			fpsCounter = 0;
		}
	}
}
