﻿using UnityEngine;


public class RandomAnimationOffset : MonoBehaviour {

	[SerializeField] Animator animator = null;

	private void Awake() {
		
		int layerIndex = 0;
		var state = animator.GetCurrentAnimatorStateInfo(layerIndex);
		animator.Play(state.fullPathHash, layerIndex, Random.value);
	}
}
