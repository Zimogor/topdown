﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.Assertions;
using UnityEngine.UI;


namespace SceneManagement {

	public enum SceneType { Menu = 1, Level = 2 } // должен соответствовать BuildIndex

	public class SceneLoader : MonoBehaviour {

		[SerializeField] Image fader = null;
		[SerializeField] GameObject content = null;
		[SerializeField] GameObject loaderCamera = null;

		public Action OnMenuLoaded;
		public Action OnLevelLoaded;

		private Scene? curScene = null;

		private void Awake() {
		
			content.SetActive(false);
			SceneManager.LoadSceneAsync((int)SceneType.Menu, LoadSceneMode.Additive);
			SceneManager.sceneLoaded += OnSceneLoaded;
			SceneManager.sceneUnloaded += OnSceneUnloaded;
		}

		private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
			if (mode != LoadSceneMode.Additive) return;

			curScene = scene;
			switch((SceneType)scene.buildIndex) {
				case SceneType.Menu:
					OnMenuLoaded?.Invoke();
					break;
				case SceneType.Level:
					OnLevelLoaded?.Invoke();
					break;
				default:
					throw new NotImplementedException();
			}
		}

		private void OnSceneUnloaded(Scene scene) {
			Assert.AreEqual(scene.buildIndex, curScene?.buildIndex);

			curScene = null;
		}

		public IEnumerator LoadScene(SceneType sceneType) {

			float speed = 3.0f;
			Color color = fader.color;
			color.a = 0.0f;
			content.SetActive(true);
			loaderCamera.SetActive(false);
			while (color.a < 1.0f) {
				color.a = Mathf.Min(1.0f, color.a + Time.deltaTime * speed);
				fader.color = color;
				yield return null;
			}
			yield return SceneManager.UnloadSceneAsync(curScene.Value);
			loaderCamera.SetActive(true);
			yield return SceneManager.LoadSceneAsync((int)sceneType, LoadSceneMode.Additive);
			loaderCamera.SetActive(false);
			yield return new WaitForSeconds(0.2f); // аниматорам время сменить кадр на случайный
			while (color.a > 0.0f) {
				color.a = Mathf.Max(0.0f, color.a - Time.deltaTime * speed);
				fader.color = color;
				yield return null;
			}
			content.SetActive(false);
		}

		private void OnDestroy() {
		
			SceneManager.sceneLoaded -= OnSceneLoaded;
			OnMenuLoaded = null;
			OnLevelLoaded = null;
		}
	}
}
