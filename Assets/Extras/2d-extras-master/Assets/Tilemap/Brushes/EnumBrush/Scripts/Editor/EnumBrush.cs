﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEditor;
using System;
using Random = UnityEngine.Random;


namespace UnityEditor {

	[Serializable]
	public struct Prefabs {

		public GameObject prefab;
		public bool oddLineDisplacement;
		public Vector2 positionScatter;
		public Vector2 scaleScatter;
		public Vector2 squashScatter;
	}

	[CreateAssetMenu(fileName = "Enum brush", menuName = "Brushes/Enum brush")]
    [CustomGridBrush(false, true, false, "Enum Brush")]
	public class EnumBrush : GridBrush {

		public Prefabs[] prefabs = null;

		[NonSerialized]
		public Prefabs? curPrefab;

		public override void Paint(GridLayout grid, GameObject brushTarget, Vector3Int position) {
			Assert.AreEqual(brushTarget.name, "Tilemap");
			Assert.IsNotNull(curPrefab.Value.prefab);

			Transform parentTransform = brushTarget.transform.Find(curPrefab.Value.prefab.name);
			if (!parentTransform) {

				parentTransform = new GameObject(curPrefab.Value.prefab.name).transform;
				parentTransform.SetParent(brushTarget.transform);
			}

			for (int i = parentTransform.childCount - 1; i >= 0; i --) {

				var child = parentTransform.GetChild(i).gameObject;
				var childPos = grid.WorldToCell(child.transform.position);
				if (childPos == position) Undo.DestroyObjectImmediate(child);
			}

			var prefabPos = GridToWorld(grid, position);
			var gameObject = (GameObject)PrefabUtility.InstantiatePrefab(curPrefab.Value.prefab, parentTransform);
			Randomize(gameObject);
			gameObject.transform.position = prefabPos;
			Undo.RegisterCreatedObjectUndo(gameObject, "Enum Prefabs");
		}

		private Vector3 GridToWorld(GridLayout grid, Vector3Int position) {

			Vector3 result;
			if (curPrefab.Value.oddLineDisplacement) {
				if (position.y % 2 == 0)
					result = grid.CellToWorld(position) + new Vector3(0.75f, 0.0f, 0.5f);
				else
					result = grid.CellToWorld(position) + new Vector3(0.25f, 0.0f, 0.5f);
			} else
				result = grid.CellToWorld(position) + new Vector3(0.5f, 0.0f, 0.5f);

			var scatter = curPrefab.Value.positionScatter;
			result.x += Random.Range(-scatter.x, scatter.x);
			result.z += Random.Range(-scatter.y, scatter.y);

			return result;
		}

		private void Randomize(GameObject gameObject) {

			var transform = gameObject.transform.Find("Sprite") ?? gameObject.transform.Find("Sprites");
			var scaleScatter = curPrefab.Value.scaleScatter;
			var squashScatter = curPrefab.Value.squashScatter;
			float size = Random.Range(scaleScatter.x, scaleScatter.y);
			float squash = Random.Range(squashScatter.x, squashScatter.y);
			var scale = transform.localScale;
			scale.x *= size;
			scale.y *= squash;
			transform.localScale = scale;
		}

		public override void Erase(GridLayout gridLayout, GameObject brushTarget, Vector3Int position) {
			
			Assert.AreEqual(brushTarget.name, "Tilemap");
			foreach (Transform container in brushTarget.transform)
				for (int i = container.childCount - 1; i >= 0; i --) {

					var child = container.GetChild(i).gameObject;
					var childPos = gridLayout.WorldToCell(child.transform.position);
					if (childPos == position) Undo.DestroyObjectImmediate(child);
				}
		}
	}

	[CustomEditor(typeof(EnumBrush))]
    public class EnumBrushEditor : GridBrushEditor {

		private string[] options = null;
		private int curOption = -1;

		public override void OnPaintInspectorGUI() {
			base.OnPaintInspectorGUI();
			var brush = target as EnumBrush;
			if (!brush) return;

			if (options == null || options.Length != brush.prefabs.Length) {
				options = new string[brush.prefabs.Length];
				curOption = -1;
			}

			for (int i = 0; i < options.Length; i ++) {
				var prefab = brush.prefabs[i].prefab;
				var prefabName = prefab ? prefab.name : "no prefab";
				options[i] = $"{i}: {prefabName}";
			}

			curOption = EditorGUILayout.Popup(curOption, options);
			if (curOption == -1 && options.Length > 0) curOption = 0;
			brush.curPrefab = curOption == -1 ? null as Prefabs? : brush.prefabs[curOption];
		}
	}
}